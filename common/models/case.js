'use strict';

module.exports = function(Case) {
	console.log('supports isNewInstance==>');
	
	Case.observe('before save', function(ctx, next) {
		console.log('supports isNewInstance?', ctx.instance);
		console.log('supports isNewInstance?', ctx.isNewInstance);
		var jsforce = require('jsforce');
		var conn = new jsforce.Connection({
						  // you can change loginUrl to connect to sandbox or prerelease env.
						  loginUrl : 'https://test.salesforce.com'
						});
		conn.login('sfdc.admin@ironsystems.com.ironcdc', 'mansasys123', function(err, res) {
		  if (err) { return console.error(err); }
			// Single record creation
			//Name : ctx.instance.ServiceGlobalAccountName,
			//Name : ctx.instance.ServiceGlobalAccountID,
			//Name : ctx.instance.ServiceGlobalProgramReferenceCode,
			// CustomerAppointment_Schedule_StartDate__c : ctx.instance.PreScheduleDate,
			// Appointment_Schedule_Start_hour__c : ctx.instance.PreScheduleHour,
			// Appointment_Schedule_Start_Minute__c : ctx.instance.PreScheduleMinute,				
			conn.sobject("Case").create(
			{ 
							
				Partner_Case_Number__c : ctx.instance.PartnerCaseNumber,
				Case_Summary__c : ctx.instance.CaseSummary,
				Description : ctx.instance.CaseDescription,
				X3PS_Vendor_Special_Instructions__c : ctx.instance.DispatchPrivateInstructions,
				Project_SOP__c : 'a1x0j0000000wb4',
				JobSite__c : 'a2q0j00000014ig',
				Ship_to_Contact_Name__c : ctx.instance.JSShipToName,
				Ship_To_Contact_Email_Address__c : ctx.instance.JSShipToEmail,
				Ship_to_Contact_Phone_Number__c : ctx.instance.JSShipToPhone,
				Customer_Service_Type__c : ctx.instance.ServiceType,
				Talent_Type__c : ctx.instance.TalentType,
				Service_Technical_Level__c : ctx.instance.TechnicalLevel,
				PPE_Hours__c : ctx.instance.EventDuration,
				Dispatch_SLA_Priority__c : ctx.instance.SLAPriority,
				
				RecordtypeId : '0121a0000006QkA'
				
			}, function(err, ret) {
			  if (err || !ret.success) { return console.error(err, ret); }
			  console.log("Created record id : " + ret.id);
			  // ...
			});
		});
		next();
	});
};


