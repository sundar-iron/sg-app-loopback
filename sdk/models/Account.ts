/* tslint:disable */

declare var Object: any;
export interface AccountInterface {
  "ServiceGlobalAccountName"?: string;
  "ServiceGlobalAccountID"?: string;
  "ServiceGlobalProgramReferenceCode"?: string;
  "ProgramName"?: string;
  "ProgramId"?: string;
  "id"?: number;
}

export class Account implements AccountInterface {
  "ServiceGlobalAccountName": string;
  "ServiceGlobalAccountID": string;
  "ServiceGlobalProgramReferenceCode": string;
  "ProgramName": string;
  "ProgramId": string;
  "id": number;
  constructor(data?: AccountInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Account`.
   */
  public static getModelName() {
    return "Account";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Account for dynamic purposes.
  **/
  public static factory(data: AccountInterface): Account{
    return new Account(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Account',
      plural: 'Accounts',
      path: 'Accounts',
      properties: {
        "ServiceGlobalAccountName": {
          name: 'ServiceGlobalAccountName',
          type: 'string'
        },
        "ServiceGlobalAccountID": {
          name: 'ServiceGlobalAccountID',
          type: 'string'
        },
        "ServiceGlobalProgramReferenceCode": {
          name: 'ServiceGlobalProgramReferenceCode',
          type: 'string'
        },
        "ProgramName": {
          name: 'ProgramName',
          type: 'string'
        },
        "ProgramId": {
          name: 'ProgramId',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
