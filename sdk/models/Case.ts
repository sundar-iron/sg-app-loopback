/* tslint:disable */

declare var Object: any;
export interface CaseInterface {
  "ServiceGlobalAccountName"?: string;
  "ServiceGlobalAccountID"?: string;
  "ServiceGlobalProgramReferenceCode"?: string;
  "PartnerCaseNumber"?: string;
  "CaseSummary"?: string;
  "CaseDescription"?: string;
  "DispatchPrivateInstructions"?: string;
  "Program"?: string;
  "ProgramID"?: string;
  "JobsiteLocationName"?: string;
  "JobsiteLocationNameID"?: string;
  "JSShipToName"?: string;
  "JSShipToEmail"?: string;
  "JSShipToPhone"?: string;
  "ServiceType"?: string;
  "TalentType"?: string;
  "TechnicalLevel"?: string;
  "EventDuration"?: string;
  "SLAPriority"?: string;
  "CustAppSetupRequest"?: boolean;
  "DispatchNow"?: boolean;
  "PreScheduled"?: boolean;
  "PreScheduleDate"?: Date;
  "PreScheduleHour"?: string;
  "PreScheduleMinute"?: string;
  "id"?: number;
}

export class Case implements CaseInterface {
  "ServiceGlobalAccountName": string;
  "ServiceGlobalAccountID": string;
  "ServiceGlobalProgramReferenceCode": string;
  "PartnerCaseNumber": string;
  "CaseSummary": string;
  "CaseDescription": string;
  "DispatchPrivateInstructions": string;
  "Program": string;
  "ProgramID": string;
  "JobsiteLocationName": string;
  "JobsiteLocationNameID": string;
  "JSShipToName": string;
  "JSShipToEmail": string;
  "JSShipToPhone": string;
  "ServiceType": string;
  "TalentType": string;
  "TechnicalLevel": string;
  "EventDuration": string;
  "SLAPriority": string;
  "CustAppSetupRequest": boolean;
  "DispatchNow": boolean;
  "PreScheduled": boolean;
  "PreScheduleDate": Date;
  "PreScheduleHour": string;
  "PreScheduleMinute": string;
  "id": number;
  constructor(data?: CaseInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Case`.
   */
  public static getModelName() {
    return "Case";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Case for dynamic purposes.
  **/
  public static factory(data: CaseInterface): Case{
    return new Case(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Case',
      plural: 'Cases',
      path: 'Cases',
      properties: {
        "ServiceGlobalAccountName": {
          name: 'ServiceGlobalAccountName',
          type: 'string'
        },
        "ServiceGlobalAccountID": {
          name: 'ServiceGlobalAccountID',
          type: 'string'
        },
        "ServiceGlobalProgramReferenceCode": {
          name: 'ServiceGlobalProgramReferenceCode',
          type: 'string'
        },
        "PartnerCaseNumber": {
          name: 'PartnerCaseNumber',
          type: 'string'
        },
        "CaseSummary": {
          name: 'CaseSummary',
          type: 'string'
        },
        "CaseDescription": {
          name: 'CaseDescription',
          type: 'string'
        },
        "DispatchPrivateInstructions": {
          name: 'DispatchPrivateInstructions',
          type: 'string'
        },
        "Program": {
          name: 'Program',
          type: 'string'
        },
        "ProgramID": {
          name: 'ProgramID',
          type: 'string'
        },
        "JobsiteLocationName": {
          name: 'JobsiteLocationName',
          type: 'string'
        },
        "JobsiteLocationNameID": {
          name: 'JobsiteLocationNameID',
          type: 'string'
        },
        "JSShipToName": {
          name: 'JSShipToName',
          type: 'string'
        },
        "JSShipToEmail": {
          name: 'JSShipToEmail',
          type: 'string'
        },
        "JSShipToPhone": {
          name: 'JSShipToPhone',
          type: 'string'
        },
        "ServiceType": {
          name: 'ServiceType',
          type: 'string'
        },
        "TalentType": {
          name: 'TalentType',
          type: 'string'
        },
        "TechnicalLevel": {
          name: 'TechnicalLevel',
          type: 'string'
        },
        "EventDuration": {
          name: 'EventDuration',
          type: 'string'
        },
        "SLAPriority": {
          name: 'SLAPriority',
          type: 'string'
        },
        "CustAppSetupRequest": {
          name: 'CustAppSetupRequest',
          type: 'boolean'
        },
        "DispatchNow": {
          name: 'DispatchNow',
          type: 'boolean'
        },
        "PreScheduled": {
          name: 'PreScheduled',
          type: 'boolean'
        },
        "PreScheduleDate": {
          name: 'PreScheduleDate',
          type: 'Date'
        },
        "PreScheduleHour": {
          name: 'PreScheduleHour',
          type: 'string'
        },
        "PreScheduleMinute": {
          name: 'PreScheduleMinute',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
