/* tslint:disable */

declare var Object: any;
export interface JobsiteProjectInterface {
  "jobsiteProjectId"?: string;
  "jobsiteId"?: string;
  "jobsiteName"?: string;
  "ProjectId"?: string;
  "id"?: number;
}

export class JobsiteProject implements JobsiteProjectInterface {
  "jobsiteProjectId": string;
  "jobsiteId": string;
  "jobsiteName": string;
  "ProjectId": string;
  "id": number;
  constructor(data?: JobsiteProjectInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `JobsiteProject`.
   */
  public static getModelName() {
    return "JobsiteProject";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of JobsiteProject for dynamic purposes.
  **/
  public static factory(data: JobsiteProjectInterface): JobsiteProject{
    return new JobsiteProject(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'JobsiteProject',
      plural: 'JobsiteProjects',
      path: 'JobsiteProjects',
      properties: {
        "jobsiteProjectId": {
          name: 'jobsiteProjectId',
          type: 'string'
        },
        "jobsiteId": {
          name: 'jobsiteId',
          type: 'string'
        },
        "jobsiteName": {
          name: 'jobsiteName',
          type: 'string'
        },
        "ProjectId": {
          name: 'ProjectId',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
